<?php
namespace Intervals;

/**
 * @package \Intervals
 */
class Interval
{
    /**
     * @var int
     */
    protected $leftEndpoint;

    /**
     * @var int
     */
    protected $rightEndpoint;

    /**
     * Constructor.
     * @param int    left endpoint
     * @param int    right endpoint
     * @return void
     * @throws \InvalidArgumentException
     */
    public function __construct($a, $b)
    {
        if (!is_int($a)) {
            throw new \InvalidArgumentException("The \$a argument must be an integer");
        }

        if (!is_int($b)) {
            throw new \InvalidArgumentException("The \$b argument must be an integer");
        }

        if ($a > $b) {
            throw new \InvalidArgumentException("The \$a argument must be less than or equal to the \$b argument");
        }

        $this->leftEndpoint  = $a;
        $this->rightEndpoint = $b;
    }

    /**
     * @return int
     */
    public function getLeft()
    {
        return $this->leftEndpoint;
    }

    /**
     * @return int
     */
    public function getRight()
    {
        return $this->rightEndpoint;
    }

    /**
     * @param \Intervals\Interval
     * @return boolean
     */
    public function isMoreGreedyThan(Interval $i)
    {
        return $this->getRight() >= $i->getLeft();
    }

    /**
     * @param \Intervals\Interval
     * @return boolean
     */
    public function canDigest(Interval $i) {
        return $this->getRight() > $i->getRight();
    }
}
