<?php
namespace Intervals;

use Closure;
use SplMinHeap;

/**
 * @package \Intervals
 */
class Intervals extends SplMinHeap
{
    /**
     * @var array
     */
    protected $backup = [];

    /**
     * @var \Closure
     */
    protected $parseLeft;

    /**
     * @var \Closure
     */
    protected $parseRight;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->parseLeft  = function($i) { return $i[0]; };
        $this->parseRight = function($i) { return $i[1]; };
    }

    /**
     * @param mixed $i
     * @param \Closure $left
     * @param \Closure $right
     * @return \Intervals\intervals
     */
    public function insert($i, Closure $left = null, Closure $right = null)
    {
        if (!($i instanceof Interval)) {
            $l = $left ?: $this->parseLeft;
            $r = $right ?: $this->parseRight;

            $i = new Interval($l($i), $r($i));
        }

        $this->backup[] = $i;

        parent::insert($i);

        return $this;
    }

    /**
     * @param array $array
     * @return \Intervals\intervals
     */
    public function inserts(array $array)
    {
        foreach($array as $value)
        {
            $this->insert($value);
        }

        return $this;
    }

    /**
     * @return boolean
     */
    public function overlaps()
    {
        $overlaps = false;

        $g = $this->extract();
        foreach($this as $i)
        {
            if( ! $g->isMoreGreedyThan($i)) {
                $g = $i;
                continue;
            }

            if( ! $g->canDigest($i)) {
                $overlaps = true;
                break;
            }
        }

        $this->rebuild();
        return $overlaps;
    }

    /**
     * @return boolean
     */
    public function containsNFirstIntegers()
    {
        $boundaries = $this->boundaries();
        $n = max($boundaries);
        return array_sum($boundaries) == $n*($n+1)/2;
    }

    /**
     * @return array
     */
    public function boundaries()
    {
        $boundaries = [];
        foreach($this as $i)
        {
            $boundaries[] = $i->getLeft();
            $boundaries[] = $i->getRight();
        }
        $this->rebuild();
        return $boundaries;
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        return !$this->overlaps() && $this->containsNFirstIntegers();
    }

    /**
     * @return \Intervals\intervals
     */
    public function clean()
    {
        foreach($this as $i);
        $this->backup = [];
        return $this;
    }

    /**
     * @param \Closure $left
     * @param \Closure $right
     * @return void
     */
    public function setDefaultEndpointsParser(Closure $left, Closure $right)
    {
        $this->parseLeft  = $left;
        $this->parseRight = $right;
    }

    /**
     * @return void
     */
    protected function rebuild()
    {
        foreach($this as $i);
        $this->inserts($this->backup);
    }

    /**
     * @param \Intervals\Interval $i
     * @param \Intervals\Interval $j
     * @return int
     */
    protected function compare (Interval $i, Interval $j)
    {
        return parent::compare($i->getLeft(), $j->getLeft());
    }
}
