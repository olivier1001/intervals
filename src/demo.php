<?php
require "../vendor/autoload.php";

use Intervals\Intervals;
use Intervals\Interval;

$intervals = new Intervals;

// $intervals->setDefaultEndpointsParser(
//     function($i) { return $i->left; },
//     function($i) { return $i->right; }
// );

echo (int) $intervals
    ->insert([1,2])
    ->inserts([[5,6], [3,4]])
    // ->insert([6,7])
    // ->insert(new Interval(3,8))
    // ->insert(call_user_func(function() {
    //     $it = new stdClass;
    //     $it->left  = 1;
    //     $it->right = 2;
    //     return $it;
    // }))
    ->isValid()
    , "\n"
;

echo (int) $intervals
    ->clean()
    ->inserts([
        [4,5],
        [6,7],
        new Interval(3,8)
    ])
    ->insert(call_user_func(function() {
        $it = new stdClass;
        $it->left  = 1;
        $it->right = 2;
        return $it;
    }), function($i) { return $i->left; }, function($i) { return $i->right; })
    ->isValid()
    , "\n"
;
exit(0);





